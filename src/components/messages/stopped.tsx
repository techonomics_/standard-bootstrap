import * as React from "react";
import { pgettext } from "tripetto-collector";

export const stoppedMessage = () => (
    <div className="row justify-content-center align-items-center message">
        <div className="col-md-8 col-lg-6">
            <div className="text-center">
                <h2>{pgettext("collector-standard-bootstrap", "✋ Stopped")}</h2>
                <h3>{pgettext("collector-standard-bootstrap", "The form was stopped")}</h3>
            </div>
        </div>
    </div>
);
