import * as React from "react";
import { ICON_EDIT } from "../icons/edit";

export const editButton = (fnEdit?: () => void) =>
    fnEdit && (
        <div className="edit" onClick={fnEdit}>
            {ICON_EDIT}
        </div>
    );
