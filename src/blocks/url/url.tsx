import * as React from "react";
import { tripetto } from "tripetto-collector";
import { URL } from "tripetto-block-url/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";

@tripetto({
    type: "node",
    identifier: "tripetto-block-url"
})
export class URLBlock extends URL implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group">
                {block.name(this.required, this.key())}
                {block.description}
                <input
                    type="url"
                    id={this.key()}
                    key={this.key()}
                    required={this.required}
                    inputMode="url"
                    defaultValue={this.urlSlot.value}
                    tabIndex={block.tabIndex}
                    placeholder={block.placeholder || "https://"}
                    className={`form-control form-control-${(block.style.form && block.style.form.inputStyle) || "default"}${
                        block.isFailed ? " is-invalid" : ""
                    }`}
                    aria-describedby={this.node.explanation && this.key("explanation")}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        this.urlSlot.value = e.target.value;
                    }}
                    onFocus={block.onFocus}
                    onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                        block.onBlur();

                        e.target.value = this.urlSlot.string;
                    }}
                />
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
