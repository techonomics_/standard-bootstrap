import * as React from "react";
import { tripetto } from "tripetto-collector";
import { Statement } from "tripetto-block-statement/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";
import { ICON } from "./icon";
import "./statement.scss";

@tripetto({
    type: "node",
    identifier: "tripetto-block-statement",
    alias: "statement"
})
export class StatementBlock extends Statement implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div
                className={`form-group tripetto-collector-statement statement-${(block.style.form && block.style.form.inputStyle) ||
                    "primary"}`}
            >
                <span>{ICON}</span>
                <span>
                    {this.props.imageAboveText && block.imageFromURL(this.props.imageURL, this.props.imageWidth)}
                    <h2>{block.name()}</h2>
                    {block.description}
                    {!this.props.imageAboveText && block.imageFromURL(this.props.imageURL, this.props.imageWidth)}
                </span>
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
