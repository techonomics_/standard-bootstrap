import * as React from "react";
import { tripetto } from "tripetto-collector";
import { IMatrixColumn, IMatrixRow, Matrix } from "tripetto-block-matrix/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";
import "./matrix.scss";

@tripetto({
    type: "node",
    identifier: "tripetto-block-matrix"
})
export class MatrixBlock extends Matrix implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group tripetto-collector-matrix">
                {block.name(this.required)}
                {block.description}
                {this.props.rows.length > 0 && this.props.columns.length > 0 && (
                    <div className="matrix-table table-responsive">
                        <table className="table">
                            <thead>
                                <tr className={`table-${(block.style.form && block.style.form.inputStyle) || "default"}`}>
                                    <th scope="col" />
                                    {this.props.columns.map((column: IMatrixColumn) => (
                                        <th key={this.key(column.id)}>{column.label}</th>
                                    ))}
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.rows.map((row: IMatrixRow) => (
                                    <tr key={this.key(row.id)}>
                                        <th scope="row">
                                            {row.name}
                                            {!this.props.required && this.isRowRequired(row) && <span className="required" />}
                                            {row.explanation && (
                                                <small className="form-text text-faded" id={this.key(`explanation-${row.id}`)}>
                                                    {row.explanation}
                                                </small>
                                            )}
                                        </th>
                                        {this.props.columns.map((column: IMatrixColumn) => (
                                            <td key={this.key(row.id + column.id)}>
                                                <div className="custom-control custom-radio">
                                                    <input
                                                        type="radio"
                                                        key={this.key(row.id + column.id)}
                                                        id={this.key(row.id + column.id)}
                                                        tabIndex={block.tabIndex}
                                                        name={this.key(row.id)}
                                                        defaultChecked={this.isColumnSelected(row, column)}
                                                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                                            this.selectColumn(row, column);
                                                        }}
                                                        className={`custom-control-input radio-control-${(block.style.form &&
                                                            block.style.form.inputStyle) ||
                                                            "default"}`}
                                                        aria-describedby={this.node.explanation && this.key(`explanation-${row.id}`)}
                                                    />
                                                    <label className="custom-control-label" htmlFor={this.key(row.id + column.id)}>
                                                        <span className="sr-only">{`${row.name} - ${column.label}`}</span>
                                                    </label>
                                                </div>
                                            </td>
                                        ))}
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                )}
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
