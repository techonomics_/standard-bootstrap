import * as React from "react";
import { pgettext, tripetto } from "tripetto-collector";
import { YesNo } from "tripetto-block-yes-no/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";
import { ICON_YES } from "./icon-yes";
import { ICON_NO } from "./icon-no";

@tripetto({
    type: "node",
    identifier: "tripetto-block-yes-no",
    alias: "yes-no",
    autoRender: true
})
export class YesNoBlock extends YesNo implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group">
                {this.props.imageAboveText && block.imageFromURL(this.props.imageURL, this.props.imageWidth)}
                {block.name(this.required)}
                {block.description}
                {!this.props.imageAboveText && block.imageFromURL(this.props.imageURL, this.props.imageWidth)}
                <div className="btn-group" role="group">
                    <button
                        key={this.key("yes")}
                        tabIndex={block.tabIndex}
                        className={`btn btn-${
                            this.answerSlot.value === "yes"
                                ? `${(block.style.form && block.style.form.positiveStyle) || "success"}`
                                : `${(block.style.form && block.style.form.neutralStyle) || "outline-secondary"}`
                        }`}
                        onClick={() => this.toggle("yes")}
                    >
                        {this.props.altYes ? (
                            this.props.altYes
                        ) : (
                            <>
                                {ICON_YES}
                                <span className="ml-2">{pgettext("collector-standard-bootstrap", "Yes")}</span>
                            </>
                        )}
                    </button>
                    <button
                        key={this.key("no")}
                        tabIndex={block.tabIndex}
                        className={`btn btn-${
                            this.answerSlot.value === "no"
                                ? `${(block.style.form && block.style.form.negativeStyle) || "danger"}`
                                : `${(block.style.form && block.style.form.neutralStyle) || "outline-secondary"}`
                        }`}
                        onClick={() => this.toggle("no")}
                    >
                        {this.props.altNo ? (
                            this.props.altNo
                        ) : (
                            <>
                                {ICON_NO}
                                <span className="ml-2">{pgettext("collector-standard-bootstrap", "No")}</span>
                            </>
                        )}
                    </button>
                </div>
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
