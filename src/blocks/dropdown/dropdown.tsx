import * as React from "react";
import { tripetto } from "tripetto-collector";
import { Dropdown, IDropdownOption } from "tripetto-block-dropdown/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";

@tripetto({
    type: "node",
    identifier: "tripetto-block-dropdown"
})
export class DropdownBlock extends Dropdown implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group">
                {block.name(this.required, this.key())}
                {block.description}
                <select
                    key={this.key()}
                    id={this.key()}
                    tabIndex={block.tabIndex}
                    defaultValue={this.value}
                    className={`custom-select form-control-${(block.style.form && block.style.form.inputStyle) || "default"}${
                        block.isFailed ? " is-invalid" : ""
                    }`}
                    aria-describedby={this.node.explanation && this.key("explanation")}
                    onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
                        this.value = e.target.value;
                    }}
                    onFocus={block.onFocus}
                    onBlur={block.onBlur}
                >
                    {block.placeholder && (
                        <option value="" className="placeholder">
                            {block.placeholder}
                        </option>
                    )}
                    {this.props.options.map(
                        (option: IDropdownOption) =>
                            option.name && (
                                <option key={option.id} value={option.id}>
                                    {option.name}
                                </option>
                            )
                    )}
                </select>
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
