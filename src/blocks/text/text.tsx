import * as React from "react";
import { tripetto } from "tripetto-collector";
import { Text } from "tripetto-block-text/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";

@tripetto({
    type: "node",
    identifier: "tripetto-block-text"
})
export class TextBlock extends Text implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group">
                {block.name(this.required, this.key())}
                {block.description}
                <input
                    type="text"
                    id={this.key()}
                    key={this.key()}
                    required={this.required}
                    defaultValue={this.textSlot.value}
                    tabIndex={block.tabIndex}
                    placeholder={block.placeholder}
                    maxLength={this.maxLength}
                    className={`form-control form-control-${(block.style.form && block.style.form.inputStyle) || "default"}${
                        block.isFailed ? " is-invalid" : ""
                    }`}
                    aria-describedby={this.node.explanation && this.key("explanation")}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        this.textSlot.value = e.target.value;
                    }}
                    onFocus={block.onFocus}
                    onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                        block.onBlur();

                        e.target.value = this.textSlot.string;
                    }}
                />
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
