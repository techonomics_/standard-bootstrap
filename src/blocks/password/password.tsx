import * as React from "react";
import { tripetto } from "tripetto-collector";
import { Password } from "tripetto-block-password/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";

@tripetto({
    type: "node",
    identifier: "tripetto-block-password"
})
export class PasswordBlock extends Password implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group">
                {block.name(this.required, this.key())}
                {block.description}
                <input
                    type="password"
                    id={this.key()}
                    key={this.key()}
                    required={this.required}
                    defaultValue={this.passwordSlot.value}
                    tabIndex={block.tabIndex}
                    placeholder={block.placeholder}
                    className={`form-control form-control-${(block.style.form && block.style.form.inputStyle) || "default"}${
                        block.isFailed ? " is-invalid" : ""
                    }`}
                    aria-describedby={this.node.explanation && this.key("explanation")}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        this.passwordSlot.value = e.target.value;
                    }}
                    onFocus={block.onFocus}
                    onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                        block.onBlur();

                        e.target.value = this.passwordSlot.string;
                    }}
                />
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
