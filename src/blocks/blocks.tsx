import * as React from "react";
import {
    Collector as CollectorFactory,
    ICollectorResumeEvent,
    IObservableNode,
    Moment,
    castToBoolean,
    extend,
    isString,
    markdownifyToString
} from "tripetto-collector";
import { Collector as CollectorComponent } from "../collector";
import { ICollectorState } from "../interfaces/state";
import { ICollectorSnapshot } from "../interfaces/snapshot";
import { IBlockRenderer } from "../interfaces/renderer";
import { markdownToJSX } from "../helpers/markdown";
import { buttons } from "../components/buttons";
import { progressbar } from "../components/progressbar";
import { pagination } from "../components/pagination";
import { editButton } from "../components/buttons/edit";
import { evaluatingMessage } from "../components/messages/evaluating";

// Import blocks with a visual representation
import "./checkbox/checkbox";
import "./checkboxes/checkboxes";
import "./dropdown/dropdown";
import "./email/email";
import "./file-upload/file-upload";
import "./matrix/matrix";
import "./multiple-choice/multiple-choice";
import "./number/number";
import "./paragraph/paragraph";
import "./password/password";
import "./radiobuttons/radiobuttons";
import "./rating/rating";
import "./statement/statement";
import "./text/text";
import "./textarea/textarea";
import "./url/url";
import "./yes-no/yes-no";

// Import headless blocks
import "tripetto-block-device/collector";
import "tripetto-block-mailer/collector";
import "tripetto-block-hidden-field/collector";

export class Blocks extends CollectorFactory<IBlockRenderer, ICollectorSnapshot> {
    private readonly component: CollectorComponent;

    constructor(component: CollectorComponent) {
        super(
            component.props.definition,
            (component.props.style && component.props.style.mode) || component.props.mode,
            component.props.snapshot || true
        );

        this.component = component;
        this.preview = component.props.view === "preview";

        this.hook("OnStart", "synchronous", () => this.reset());
        this.hook("OnRestart", "synchronous", () => this.reset());
        this.hook("OnResume", "synchronous", (event: ICollectorResumeEvent<ICollectorSnapshot>) => {
            if (event.data && event.data.b) {
                component.setState({
                    focus: event.data.b || {}
                });
            }
        });
    }

    private reset(): void {
        if (!this.component.isReloading) {
            this.component.setState({
                changes: 0,
                focus: {}
            });
        }
    }

    private handleRef(key: string, el: HTMLElement | null): void {
        if (el && this.component.state.activate === key) {
            this.component.setState({
                activate: undefined
            });

            el.scrollIntoView({
                behavior: "smooth",
                block: "center"
            });
        }
    }

    private setFocus(key: string, focus: boolean): void {
        this.component.setState((prevState: ICollectorState) => ({
            focus: extend(prevState, {
                [key]: focus
            })
        }));
    }

    render(): React.ReactNode {
        const storyline = this.storyline;
        const style = this.component.style;
        const showEnumerators = castToBoolean(style.showEnumerators);
        const isPreview = this.isPreview;
        const focus = this.component.state.focus;
        let tabIndex = 0;

        return (
            storyline &&
            !storyline.isEmpty && (
                <>
                    {storyline.map((moment: Moment<IBlockRenderer>, momentIndex: number) =>
                        moment.nodes.map((node: IObservableNode<IBlockRenderer>, nodeIndex: number) => {
                            const fnEdit =
                                (this.component.props.onEditRequest &&
                                    this.component.props.usage === "preview" &&
                                    this.component.view !== "normal" &&
                                    node.id &&
                                    (() => this.component.props.onEditRequest!(node.id))) ||
                                undefined;
                            return node.block ? (
                                <div key={node.key} ref={(el: HTMLElement | null) => this.handleRef(node.key, el)}>
                                    {node.block.render({
                                        style,
                                        name: (required?: boolean, labelFor?: string) =>
                                            isString(node.props.name) &&
                                            castToBoolean(node.props.nameVisible, true) && (
                                                <label htmlFor={labelFor} onClick={(!labelFor && fnEdit) || undefined}>
                                                    {showEnumerators && node.enumerator && `${node.enumerator}. `}
                                                    {markdownToJSX(node.props.name || "...", node.context)}
                                                    {required && <span className="required" />}
                                                </label>
                                            ),
                                        get description(): React.ReactNode {
                                            return (
                                                node.props.description && (
                                                    <p className="text-secondary" onClick={fnEdit}>
                                                        {markdownToJSX(node.props.description, node.context)}
                                                    </p>
                                                )
                                            );
                                        },
                                        imageFromURL: (imageURL?: string, imageWidth?: string) =>
                                            imageURL && (
                                                <img
                                                    src={imageURL}
                                                    onLoad={() => this.component.forceUpdate()}
                                                    className="img-fluid rounded-lg my-3"
                                                    style={{
                                                        width: imageWidth
                                                    }}
                                                    onClick={fnEdit}
                                                />
                                            ),
                                        explanation: (label?: string) =>
                                            node.props.explanation && (
                                                <small className="form-text text-faded" id={label} onClick={fnEdit}>
                                                    {markdownToJSX(node.props.explanation, node.context)}
                                                </small>
                                            ),
                                        get placeholder(): string {
                                            return markdownifyToString(node.props.placeholder || "", node.context, "...");
                                        },
                                        placeholderOrName: (required?: boolean) => (
                                            <>
                                                {markdownToJSX(node.props.placeholder || node.props.name || "...", node.context, false)}
                                                {required && <span className="required" />}
                                            </>
                                        ),
                                        get tabIndex(): number {
                                            return ++tabIndex;
                                        },
                                        get isFailed(): boolean {
                                            return !isPreview && node.isFailed && focus[node.key] === false;
                                        },
                                        update: () => this.component.forceUpdate(),
                                        onFocus: () => this.setFocus(node.key, true),
                                        onBlur: () => this.setFocus(node.key, false),
                                        onAttachment: this.component.props.onAttachment
                                    })}
                                    {editButton(fnEdit)}
                                </div>
                            ) : (
                                <div
                                    key={node.key}
                                    ref={(el: HTMLElement | null) => this.handleRef(node.key, el)}
                                    className={momentIndex === 0 && nodeIndex === 0 ? "" : "separate"}
                                >
                                    {castToBoolean(node.props.nameVisible, true) && (
                                        <h3 onClick={fnEdit}>{markdownToJSX(node.props.name || "...", node.context)}</h3>
                                    )}
                                    {node.props.description && (
                                        <p className="text-secondary" onClick={fnEdit}>
                                            {markdownToJSX(node.props.description, node.context, true)}
                                        </p>
                                    )}
                                    {editButton(fnEdit)}
                                </div>
                            );
                        })
                    )}

                    {!this.isPreview &&
                        (storyline.isEvaluating ? (
                            evaluatingMessage()
                        ) : (
                            <nav className="row navigation justify-content-between align-items-center">
                                {buttons(
                                    storyline,
                                    style,
                                    this.component.view,
                                    this.component.props.onPause && (() => this.component.requestPause())
                                )}
                                {castToBoolean(style.showProgressbar, true) && progressbar(this.mode, storyline, style)}
                                {style.showPageIndicators && pagination(storyline, style)}
                            </nav>
                        ))}
                </>
            )
        );
    }

    componentDidUpdate(): void {
        this.preview = this.component.state.view === "preview";
        this.mode = (this.component.state.style && this.component.state.style.mode) || this.component.state.mode;
    }
}
