import * as React from "react";
import { tripetto } from "tripetto-collector";
import { Paragraph } from "tripetto-block-paragraph/collector";
import { IBlockRenderer } from "../../interfaces/renderer";
import { IBlockHelper } from "../../interfaces/block";
import { markdownToJSX } from "../../helpers/markdown";

@tripetto({
    type: "node",
    identifier: "tripetto-block-paragraph",
    alias: "paragraph"
})
export class ParagraphBlock extends Paragraph implements IBlockRenderer {
    render(block: IBlockHelper): React.ReactNode {
        return (
            <div className="form-group">
                {this.props.imageAboveText && block.imageFromURL(this.props.imageURL, this.props.imageWidth)}
                <h2>{block.name()}</h2>
                {this.props.caption && <h3>{markdownToJSX(this.props.caption || "...", this.context)}</h3>}
                {block.description}
                {!this.props.imageAboveText && block.imageFromURL(this.props.imageURL, this.props.imageWidth)}
                {block.explanation(this.key("explanation"))}
            </div>
        );
    }
}
