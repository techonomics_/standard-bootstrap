export type TFonts =
    | ""
    | "Arial, Helvetica, Sans-Serif"
    | "Arial Black, Gadget, Sans-Serif"
    | "Comic Sans MS, Textile, Cursive, Sans-Serif"
    | "Courier New, Courier, Monospace"
    | "Georgia, Times New Roman, Times, Serif"
    | "Impact, Charcoal, Sans-Serif"
    | "Lucida Console, Monaco, Monospace"
    | "Lucida Sans Unicode, Lucida Grande, Sans-Serif"
    | "Palatino Linotype, Book Antiqua, Palatino, Serif"
    | "Tahoma, Geneva, Sans-Serif"
    | "Times New Roman, Times, Serif"
    | "Trebuchet MS, Helvetica, Sans-Serif"
    | "Verdana, Geneva, Sans-Serif";
export type TBootstrapStyles = "primary" | "secondary" | "success" | "info" | "warning" | "danger" | "light" | "dark";
export type TBootstrapStylesOutline =
    | "outline-primary"
    | "outline-secondary"
    | "outline-success"
    | "outline-info"
    | "outline-warning"
    | "outline-danger"
    | "outline-light"
    | "outline-dark";

export interface ICollectorStyle {
    readonly backgroundColor?: string;
    readonly textFont?: TFonts;
    readonly textColor?: string;
    readonly mode?: "paginated" | "continuous" | "progressive";
    readonly showEnumerators?: boolean;
    readonly showPageIndicators?: boolean;
    readonly showProgressbar?: boolean;

    readonly backgroundImage?: {
        readonly url: string;
        readonly size: "auto" | "100% auto" | "auto 100%" | "cover" | "contain" | "repeat";
    };

    readonly form?: {
        readonly inputStyle: TBootstrapStyles;
        readonly unselectedStyle: TBootstrapStyles | TBootstrapStylesOutline;
        readonly selectedStyle: TBootstrapStyles | TBootstrapStylesOutline;
        readonly neutralStyle: TBootstrapStyles | TBootstrapStylesOutline;
        readonly positiveStyle: TBootstrapStyles | TBootstrapStylesOutline;
        readonly negativeStyle: TBootstrapStyles | TBootstrapStylesOutline;
    };

    readonly buttons?: {
        readonly nextStyle: TBootstrapStyles | TBootstrapStylesOutline;
        readonly nextLabel?: string;
        readonly backStyle: TBootstrapStyles | TBootstrapStylesOutline;
        readonly backLabel?: string;
        readonly completeStyle: TBootstrapStyles | TBootstrapStylesOutline;
        readonly completeLabel?: string;
        readonly pauseStyle: TBootstrapStyles | TBootstrapStylesOutline;
    };

    readonly navigation?: {
        readonly progressbarStyle: TBootstrapStyles;
        readonly paginationStyle: TBootstrapStyles;
    };
}
