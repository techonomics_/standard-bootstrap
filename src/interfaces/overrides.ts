export interface ICollectorOverrides {
    readonly removeBranding?: boolean;
    readonly confirmationTitle?: string;
    readonly confirmationSubtitle?: string;
    readonly confirmationText?: string;
    readonly confirmationImage?: string;
    readonly confirmationButton?:
        | {
              readonly label: string;
              readonly url: string;
          }
        | "off";
}
