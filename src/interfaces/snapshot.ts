export interface ICollectorSnapshot {
    readonly b?: {
        [key: string]: false | true | undefined;
    };
}
